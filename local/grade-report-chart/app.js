// This file is part of solution for Audensiel Moodle test
// More details in readme.txt
//
// Author: Karine Pires <karine@karinepires.com>
//
// For this script to work the following scripts must be included at the HEAD
// at the link: Home ▶ Site administration ▶ Appearance ▶ Additional HTML
// <link rel="stylesheet" href="/local/grade-report-chart/app.css">
// <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
// <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.3.5/angular.min.js"></script>
// <script src="https://ajax.googleapis.com/ajax/libs/angular_material/0.6.1/angular-material.min.js"></script>
// <script type="text/javascript" src="https://www.google.com/jsapi"></script>
// <script type="text/javascript" src="/local/grade-report-chart/app.js"></script>

// Draw a google API pie chart
function drawChart(chartTitle,chartData,chardID) {

  var data = google.visualization.arrayToDataTable(chartData);

  var options = {
    title: chartTitle,
    is3D: true,
  };

  var chart = new google.visualization.PieChart(document.getElementById(chardID));

  chart.draw(data, options);
};

// Generate data from DOM to the pie chart
function generateChartData(elem) {

  var chartDataRaw = [], prev;

  $(elem).each(function() { chartDataRaw.push($(this).text()); });

  chartDataRaw.sort();

  var chartData = [["Grade","Number of Students"]];

  for (var i = 0; i < chartDataRaw.length; i++) {
    if ( chartDataRaw[i] !== prev ) {
      chartData.push([chartDataRaw[i],1]);
    } else {
      chartData[chartData.length-1][1]++;
    }
    prev = chartDataRaw[i];
  };

  return chartData;

};


// Start the app when DOM is loaded
$(function () {

  // Make module Foo and store providers for later use
  var providers = {};
  angular.module('Foo', [], function($controllerProvider, $compileProvider, $provide) {
      providers = {
          $controllerProvider: $controllerProvider,
          $compileProvider: $compileProvider,
          $provide: $provide
      };
  });

  // Bootstrap Foo
  angular.bootstrap($('body'), ['Foo']);

  // Store our _invokeQueue length before loading our controllers/directives/services
  // This is just so we don't re-register anything
  var queueLen = angular.module('Foo')._invokeQueue.length;

  // Load javascript file with controllers/directives/services
  angular.module('Foo')
  .controller('Ctrl', function($scope, $rootScope) {
      $scope.generatePieChart = function(){

        // Grades of a specific quiz
        var chartData = generateChartData(".grade.i2 .gradevalue");
        drawChart("Quiz - Students per Grades",chartData,"piechart1");

        // Grades of the sum course
        chartData = generateChartData(".grade.course .gradevalue");
        drawChart("Course - Students per Grades",chartData,"piechart2");

      };
  });

  // Load html file with content which uses above content
  $('<div id="ctrl" ng-controller="Ctrl">' +
    '<button class="blue raised" data-ng-click="generatePieChart()"  >Generate Pie Chart</button>' +
    '<div id="piechart1"/>' +
    '<div id="piechart2"/>' +
  '</div>').appendTo('.gradeparent');

  // Register the controls/directives/services we just loaded
  var queue = angular.module('Foo')._invokeQueue;
  for(var i=queueLen;i<queue.length;i++) {
    var call = queue[i];
    // call is in the form [providerName, providerFunc, providerArguments]
    var provider = providers[call[0]];
    if(provider) {
        // e.g. $controllerProvider.register("Ctrl", function() { ... })
        provider[call[1]].apply(provider, call[2]);
    }
  }

  // Compile the new element
  $('body').injector().invoke(function($compile, $rootScope) {
    $compile($('#ctrl'))($rootScope);
    $rootScope.$apply();
  });


});

// Load the google api
google.load("visualization", "1", {packages:["corechart"]});


