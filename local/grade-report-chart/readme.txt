READ ME
=======

This is a AngularJS program proposed by Karine Pires that solves the test proposed by Audensiel.

The main objective is to add a pie chart to the grade page of Moodle (an open source learning platform).

Details of the exercise can be found on the original announcement (in French):

1)Installer Moodle:
    https://docs.moodle.org/28/en/Installing_Moodle#Set_up_your_server

2)Une fois installé, une base moodle sera créée. Tu peux y importer le fichier moodle.sql attaché. J'ai rempli la base avec des données.

3)Familiarise-toi avec Moodle un peu.

4)J'ai attaché 3 écrans qui sont en fait le même écran mais en vertical.
But:
    -ajouter sur la partie de gauche ("Navigation") un lien nommé "Notes" qui affiche l'écran qui se trouve dans quiz2.png, c'est-à-dire "Administration" > Administration du test > Résultats > Notes.
    -dans l'écran, avec Angularjs, ajouter un bouton "afficher camembert".
    -si on clique sur le bouton, on affiche avec google charts(https://developers.google.com/chart/) un pie chart qui donne le pourcentage d'étudiants par tranche de note.


INSTALL
=======

For the program to work the following scripts must be included at the tag HEAD.
In Moodle platform this is possible to include by the link: Home ▶ Site administration ▶ Appearance ▶ Additional HTML

<link rel="stylesheet" href="/local/grade-report-chart/app.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.3.5/angular.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/angular_material/0.6.1/angular-material.min.js"></script>
<script type="text/javascript" src="https://www.google.com/jsapi"></script>
<script type="text/javascript" src="/local/grade-report-chart/app.js"></script>

